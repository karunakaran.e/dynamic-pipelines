import os
import textwrap

# Dictionary mapping branch names to equivalent environments
branch_map = {
    'main': 'production',
    'staging': 'acceptance',
    'develop': 'develop'
}

# Function to create job definition based on environment
def create_job(environment):

  ld_ff_app_env = 'dev'
  ci_yaml_config = textwrap.dedent(f"""
        default:
        image: python:3

        stages:
        - build
        - trigger
        - deploy

        build:{ld_ff_app_env}:
            stage: build
            script: 
                - echo "build"
        trigger:{ld_ff_app_env}:
            stage: trigger
            script:
                - echo "trigger"
        deploy:{ld_ff_app_env}:
            stage: deploy
            script:
                - echo  'deploying the application to {ld_ff_app_env}'
        """)
  return ci_yaml_config

# Function to create dynamic GitLab CI file
def create_dynamic_gitlab_file():
    # Read the branch name from environment variable CI_COMMIT_REF_NAME
    ci_commit_ref_name = os.environ.get('CI_COMMIT_REF_NAME')

    # Map the branch name to the equivalent environment
    environment = branch_map.get(ci_commit_ref_name)

    # Create content of CI file
    job_content = create_job(environment)

    # Write content to file
    with open('dynamic-gitlab-ci.yml', 'w', encoding='utf-8') as file:
        file.write(job_content)

# Call the function to create the dynamic GitLab CI file
create_dynamic_gitlab_file()
